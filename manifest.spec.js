const manifest = require('./manifest.json');
const packageJson = require('./package.json');

test('Specific properties on manifest matche those on packageJson', () => {
    expect(manifest.author).toBe(packageJson.author);
    expect(manifest.name).toBe(packageJson.display_name);
    expect(manifest.id).toBe(packageJson.name);
    expect(manifest.license).toBe(packageJson.license);
    expect(manifest.version).toBe(packageJson.version);
    expect(manifest.description).toBe(packageJson.description);
    expect(manifest.homepage_url).toBe(packageJson.homepage);
});
test('manifest.options.schema matches packageJson.moziot.schema', () => {
    expect(manifest.options.schema).toEqual(packageJson.moziot.schema);
});
